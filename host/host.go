package host

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"superlive-sdk/internal"
)

type IHostClient interface {
	Create(name string, description string, username string, password string) (*internal.ResponseBody[*Host, any], error)
	Update(id string, name string, description string) (*internal.ResponseBody[string, any], error)
	GetAll(page int, limit int) (*internal.ResponseBody[[]*Host, any], error)
	GetOne(id string) (*internal.ResponseBody[*Host, any], error)
	DeleteOne(id string) (*internal.ResponseBody[string, any], error)
	Count() (*internal.ResponseBody[int, any], error)
}

type HostClient struct {
	endpoint   string
	apiKey     string
	httpClient *internal.HTTPClient
}

func NewHostClient(endpoint string, apiKey string, httpClient *internal.HTTPClient) *HostClient {
	return &HostClient{endpoint: endpoint, apiKey: apiKey, httpClient: httpClient}
}

func (hc *HostClient) Create(name string, description string, username string, password string) (*internal.ResponseBody[*Host, any], error) {
	payload := &CreateHost{
		Name:        name,
		Description: description,
		Username:    username,
		Password:    password,
	}

	req := &internal.Request{
		Method: http.MethodPost,
		URL:    hc.endpoint + "/hosts",
		Body:   internal.NewJSONEntity(payload),
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error creating host: %v", err)
	}

	var response = &internal.ResponseBody[*Host, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *HostClient) Update(id string, name string, description string) (*internal.ResponseBody[string, any], error) {
	payload := &UpdateHost{
		Name:        name,
		Description: description,
	}

	req := &internal.Request{
		Method: http.MethodPut,
		URL:    hc.endpoint + "/hosts/" + id,
		Body:   internal.NewJSONEntity(payload),
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error creating host: %v", err)
	}

	var response = &internal.ResponseBody[string, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *HostClient) GetAll(page int, limit int) (*internal.ResponseBody[[]*Host, any], error) {

	req := &internal.Request{
		Method: http.MethodGet,
		URL:    hc.endpoint + "/hosts",
		Body:   nil,
		Opts: []internal.HTTPOption{
			internal.WithQueryParams(map[string]string{
				"page": strconv.Itoa(page), "limit": strconv.Itoa(limit),
			}),
		},
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error getting all hosts: %v", err)
	}

	var response = &internal.ResponseBody[[]*Host, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *HostClient) GetOne(id string) (*internal.ResponseBody[*Host, any], error) {

	req := &internal.Request{
		Method: http.MethodGet,
		URL:    hc.endpoint + "/hosts/" + id,
		Body:   nil,
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error getting all hosts: %v", err)
	}

	var response = &internal.ResponseBody[*Host, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *HostClient) DeleteOne(id string) (*internal.ResponseBody[string, any], error) {

	req := &internal.Request{
		Method: http.MethodDelete,
		URL:    hc.endpoint + "/hosts/" + id,
		Body:   nil,
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error deleting host: %v", err)
	}

	var response = &internal.ResponseBody[string, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *HostClient) Count() (*internal.ResponseBody[int, any], error) {

	req := &internal.Request{
		Method: http.MethodGet,
		URL:    hc.endpoint + "/hosts/count",
		Body:   nil,
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error counting host: %v", err)
	}

	var response = &internal.ResponseBody[int, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}
