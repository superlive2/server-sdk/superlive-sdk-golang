package host

import "time"

type Host struct {
	Id            string       `json:"_id"`
	Name          string       `json:"name"`
	Description   string       `json:"description"`
	Username      string       `json:"username"`
	Password      string       `json:"-"`
	CreatedAt     time.Time    `json:"createdAt"`
	UpdatedAt     time.Time    `json:"updatedAt"`
	PushOptions   []PushOption `json:"pushOptions"`
	PlayOptions   []PlayOption `json:"playOptions"`
	ThumbnailUrls []string     `json:"thumbnailUrls"`
}

type PushOption struct {
	Type string `json:"type"`
	Url  string `json:"url"`
}

type PlayOption struct {
	Name string   `json:"name"`
	Urls []string `json:"urls"`
}

type CreateHost struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Username    string `json:"username"`
	Password    string `json:"password"`
}

type UpdateHost struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type HostResponse struct {
	Data *Host `json:"data"`
}

type HostsResponse struct {
	Data []*Host `json:"data"`
}
