package superlive

import (
	"superlive-sdk/host"
	"superlive-sdk/internal"
	"superlive-sdk/participant"
)

type Config struct {
	Production bool
	ApiKey     string
}

type ISusperLiveSDK interface {
	Host() host.IHostClient
	Participant() participant.IParticipantClient
}

type SusperLiveSDK struct {
	apiEndpoint       string
	apiKey            string
	hostClient        *host.HostClient
	participantClient *participant.ParticipantClient
}

func NewClient(config *Config) ISusperLiveSDK {
	apiEndpoint := ""
	if config.Production {
		apiEndpoint = "https://merchant.super-live.tv/api/sdk"
	} else {
		apiEndpoint = "http://merch.sp.tv/api/sdk"
	}
	return &SusperLiveSDK{apiEndpoint: apiEndpoint, apiKey: config.ApiKey}
}

func (sdk *SusperLiveSDK) Host() host.IHostClient {
	if sdk.hostClient == nil {
		httpClient := internal.NewHttpClient([]internal.HTTPOption{
			internal.WithHeader("Authorization", sdk.apiKey),
		})
		sdk.hostClient = host.NewHostClient(sdk.apiEndpoint, sdk.apiKey, httpClient)
	}

	return sdk.hostClient
}

func (sdk *SusperLiveSDK) Participant() participant.IParticipantClient {
	if sdk.participantClient == nil {
		httpClient := internal.NewHttpClient([]internal.HTTPOption{
			internal.WithHeader("Authorization", sdk.apiKey),
		})
		sdk.participantClient = participant.NewParticipantClient(sdk.apiEndpoint, sdk.apiKey, httpClient)
	}

	return sdk.participantClient
}
