package participant

import "time"

type Participant struct {
	Id          string    `json:"_id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}

type CreateParticipant struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type UpdateParticipant struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type ParticipantResponse struct {
	Data *Participant `json:"data"`
}

type ParticipantsResponse struct {
	Data []*Participant `json:"data"`
}
