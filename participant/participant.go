package participant

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"superlive-sdk/internal"
)

type IParticipantClient interface {
	Create(name string, description string) (*internal.ResponseBody[*Participant, any], error)
	Update(id string, name string, description string) (*internal.ResponseBody[string, any], error)
	GetAll(page int, limit int) (*internal.ResponseBody[[]*Participant, any], error)
	GetOne(id string) (*internal.ResponseBody[*Participant, any], error)
	DeleteOne(id string) (*internal.ResponseBody[string, any], error)
}

type ParticipantClient struct {
	endpoint   string
	apiKey     string
	httpClient *internal.HTTPClient
}

func NewParticipantClient(endpoint string, apiKey string, httpClient *internal.HTTPClient) *ParticipantClient {
	return &ParticipantClient{endpoint: endpoint, apiKey: apiKey, httpClient: httpClient}
}

func (hc *ParticipantClient) Create(name string, description string) (*internal.ResponseBody[*Participant, any], error) {
	payload := &CreateParticipant{
		Name:        name,
		Description: description,
	}

	req := &internal.Request{
		Method: http.MethodPost,
		URL:    hc.endpoint + "/participants",
		Body:   internal.NewJSONEntity(payload),
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error creating: %v", err)
	}

	var response = &internal.ResponseBody[*Participant, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *ParticipantClient) Update(id string, name string, description string) (*internal.ResponseBody[string, any], error) {
	payload := &UpdateParticipant{
		Name:        name,
		Description: description,
	}

	req := &internal.Request{
		Method: http.MethodPut,
		URL:    hc.endpoint + "/participants/" + id,
		Body:   internal.NewJSONEntity(payload),
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error creating: %v", err)
	}

	var response = &internal.ResponseBody[string, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *ParticipantClient) GetAll(page int, limit int) (*internal.ResponseBody[[]*Participant, any], error) {

	req := &internal.Request{
		Method: http.MethodGet,
		URL:    hc.endpoint + "/participants",
		Body:   nil,
		Opts: []internal.HTTPOption{
			internal.WithQueryParams(map[string]string{
				"page": strconv.Itoa(page), "limit": strconv.Itoa(limit),
			}),
		},
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error getting all: %v", err)
	}

	var response = &internal.ResponseBody[[]*Participant, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *ParticipantClient) GetOne(id string) (*internal.ResponseBody[*Participant, any], error) {

	req := &internal.Request{
		Method: http.MethodGet,
		URL:    hc.endpoint + "/participants/" + id,
		Body:   nil,
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error getting all: %v", err)
	}

	var response = &internal.ResponseBody[*Participant, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}

func (hc *ParticipantClient) DeleteOne(id string) (*internal.ResponseBody[string, any], error) {

	req := &internal.Request{
		Method: http.MethodDelete,
		URL:    hc.endpoint + "/participants/" + id,
		Body:   nil,
	}

	resp, err := hc.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error deleting: %v", err)
	}

	var response = &internal.ResponseBody[string, any]{}
	err = json.Unmarshal(resp.Body, response)
	if err != nil {
		return nil, fmt.Errorf("error parsing: %v", err)
	}

	return response, nil
}
