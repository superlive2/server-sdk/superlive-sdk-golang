package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

// http client
type HTTPOption func(*http.Request)

type HTTPClient struct {
	Client *http.Client
	Opts   []HTTPOption
}

func NewHttpClient(opts []HTTPOption) *HTTPClient {
	return &HTTPClient{
		Client: &http.Client{},
		Opts:   opts,
	}
}

// request
type Request struct {
	Method string
	URL    string
	Body   HTTPEntity
	Opts   []HTTPOption
}

type HTTPEntity interface {
	Bytes() ([]byte, error)
	Mime() string
}

// response
type Response struct {
	Status int
	Header http.Header
	Body   []byte
	resp   *http.Response
}

type ResponseBody[D any, E any] struct {
	StatusCode int
	Data       D
	Extra      E
}

// WithHeader creates an HTTPOption that will set an HTTP header on the request.
func WithHeader(key, value string) HTTPOption {
	return func(r *http.Request) {
		r.Header.Set(key, value)
	}
}

// WithQueryParam creates an HTTPOption that will set a query parameter on the request.
func WithQueryParam(key, value string) HTTPOption {
	return func(r *http.Request) {
		q := r.URL.Query()
		q.Add(key, value)
		r.URL.RawQuery = q.Encode()
	}
}

// WithQueryParams creates an HTTPOption that will set all the entries of qp as query parameters
// on the request.
func WithQueryParams(qp map[string]string) HTTPOption {
	return func(r *http.Request) {
		q := r.URL.Query()
		for k, v := range qp {
			q.Add(k, v)
		}
		r.URL.RawQuery = q.Encode()
	}
}

// json enity
type jsonEntity struct {
	Val interface{}
}

// NewJSONEntity creates a new HTTPEntity that will be serialized into JSON.
func NewJSONEntity(val interface{}) HTTPEntity {
	return &jsonEntity{Val: val}
}

func (e *jsonEntity) Bytes() ([]byte, error) {
	// convert struct to map
	b, err := json.Marshal(&e.Val)
	if err != nil {
		return nil, fmt.Errorf("unable to parse payload: %v", err)
	}
	var m map[string]interface{}
	_ = json.Unmarshal(b, &m)
	if err != nil {
		return nil, fmt.Errorf("unable to parse payload: %v", err)
	}
	// ---------------------

	return json.Marshal(m)
}

func (e *jsonEntity) Mime() string {
	return "application/json"
}

// build request
func (r *Request) buildHTTPRequest(opts []HTTPOption) (*http.Request, error) {
	var data io.Reader
	if r.Body != nil {
		b, err := r.Body.Bytes()
		if err != nil {
			return nil, err
		}
		data = bytes.NewBuffer(b)
		opts = append(opts, WithHeader("Content-Type", r.Body.Mime()))
	}

	req, err := http.NewRequest(r.Method, r.URL, data)
	if err != nil {
		return nil, err
	}

	opts = append(opts, r.Opts...)
	for _, o := range opts {
		o(req)
	}

	return req, nil
}

// call request
func (c *HTTPClient) Do(req *Request) (*Response, error) {
	hr, err := req.buildHTTPRequest(c.Opts)
	if err != nil {
		return nil, err
	}

	resp, err := c.Client.Do(hr)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusBadRequest {
		var errRes *ErrorResponse = &ErrorResponse{}
		err := json.Unmarshal(b, errRes)
		if err != nil {
			return nil, fmt.Errorf("error parsing (bad request): %v", err)
		}

		return nil, fmt.Errorf("bad request error, errorCode: %v, details: %v", errRes.StatusCode, errRes.Message)
	}

	if resp.StatusCode == http.StatusUnauthorized {
		var errRes *ErrorResponse = &ErrorResponse{}
		err := json.Unmarshal(b, errRes)
		if err != nil {
			return nil, fmt.Errorf("error parsing (Unauthorized): %v", err)
		}

		return nil, fmt.Errorf("Unauthorized request error, errorCode: %v, details: %v", errRes.StatusCode, errRes.Message)
	}

	if resp.StatusCode == http.StatusNotFound {
		var errRes *ErrorResponse = &ErrorResponse{}
		err := json.Unmarshal(b, errRes)
		if err != nil {
			return nil, fmt.Errorf("error parsing response: %v", err)
		}

		return nil, fmt.Errorf("Not Found request error, errorCode: %v, details: %v", errRes.StatusCode, errRes.Message)
	}

	return &Response{
		Status: resp.StatusCode,
		Header: resp.Header,
		Body:   b,
		resp:   resp,
	}, nil
}
